//
//  ViewController.swift
//  Filtros
//
//  Created by formador on 25/1/17.
//  Copyright © 2017 formador. All rights reserved.
//

import UIKit
import CoreImage

//CoreImage
//CIContext CIFilter


class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var intensidad: UISlider!
    
    var imagenActual: UIImage!
    var context: CIContext!
    var filtroActual: CIFilter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Yet Another Core Image Filter App
        title = "YACIFP"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(importarImagen))
        
        context = CIContext()
        filtroActual = CIFilter(name: "CISepiaTone")
    }
    
    func importarImagen() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let imagen = info[UIImagePickerControllerEditedImage] as? UIImage else { return }
        dismiss(animated: true)
        imagenActual = imagen
        
        let imagenInicial = CIImage(image: imagenActual)
        filtroActual.setValue(imagenInicial, forKey: kCIInputImageKey)
        aplicarProcesamiento()
    }
    
    func aplicarProcesamiento() {
        let inputKeys = filtroActual.inputKeys
        if inputKeys.contains(kCIInputIntensityKey) {
            filtroActual.setValue(intensidad.value, forKey: kCIInputIntensityKey)
        }
        if inputKeys.contains(kCIInputRadiusKey) {
            filtroActual.setValue(intensidad.value * 200, forKey: kCIInputRadiusKey)
        }
        if inputKeys.contains(kCIInputScaleKey) {
            filtroActual.setValue(intensidad.value * 10, forKey: kCIInputScaleKey)
        }
        if inputKeys.contains(kCIInputCenterKey) {
            filtroActual.setValue(CIVector(x: imagenActual.size.width / 2, y: imagenActual.size.height / 2), forKey: kCIInputCenterKey)
        }
        
        //filtroActual.setValue(intensidad.value, forKey: kCIInputIntensityKey)
        if let cgimg = context.createCGImage(filtroActual.outputImage!, from: filtroActual.outputImage!.extent) {
            let imagenProcesada = UIImage(cgImage: cgimg)
            imageView.image = imagenProcesada
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cambiarFiltro(_ sender: AnyObject) {
        let ac = UIAlertController(title: "Elige tu filtro", message: nil, preferredStyle: .actionSheet)
        ac.addAction(UIAlertAction(title: "CIBumpDistortion", style: .default, handler: establecerFiltro))
        ac.addAction(UIAlertAction(title: "CIGaussianBlur", style: .default, handler: establecerFiltro))
        ac.addAction(UIAlertAction(title: "CIPixellate", style: .default, handler: establecerFiltro))
        ac.addAction(UIAlertAction(title: "CISepiaTone", style: .default, handler: establecerFiltro))
        ac.addAction(UIAlertAction(title: "CITwirlDistortion", style: .default, handler: establecerFiltro))
        ac.addAction(UIAlertAction(title: "CIUnsharpMask", style: .default, handler: establecerFiltro))
        ac.addAction(UIAlertAction(title: "CIVignette", style: .default, handler: establecerFiltro))
        ac.addAction(UIAlertAction(title: "Cancelar", style: .cancel))
        present(ac, animated: true)
    }
    
    func establecerFiltro(action: UIAlertAction) {
        guard imagenActual != nil else { return }
        filtroActual = CIFilter(name: action.title!)
        
        let imagenInicial = CIImage(image: imagenActual)
        filtroActual.setValue(imagenInicial, forKey: kCIInputImageKey)
        aplicarProcesamiento()
    }
    

    @IBAction func guardar(_ sender: AnyObject) {
        //#selector(metodoALlamar)
        UIImageWriteToSavedPhotosAlbum(imageView.image!, self, #selector(image), nil)
        
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Error al guardar", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Ok", style: .default))
                present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Imagen Guardada", message: "Tu imagen modificada ha sido guardada con éxito", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Ok", style: .default))
            present(ac, animated: true)
        }
    }
    
    @IBAction func cambiarIntensidad(_ sender: AnyObject) {
        aplicarProcesamiento()
    }
    
    
}

